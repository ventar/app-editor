function sendEditor() {

  var docH = $(document).height(),
    winW = $(document).width(),
    titleH = $("#send-title").outerHeight(true),
    navH = $(".nav").outerHeight(true),
    navdH = $(".nav>div").outerHeight(true),
    menuH = $(".action-menu").outerHeight(true),
    listH = (winW / 750) * 512;

  function minH() {
    if (winW >= 320) {
      $('body').css('height', docH - listH - titleH - 40);
      $('.nav').css('height', docH - titleH * 2 - listH - menuH - 40);
      $('#send-content').css('height', docH - titleH * 2 - listH - navdH - menuH - 40);
    }
    if (winW >= 360) {
      $('body').css('height', docH - listH - titleH);
      $('.nav').css('height', docH - titleH * 2 - listH - menuH);
      $('#send-content').css('height', docH - titleH * 2 - listH - navdH - menuH);
    }
    if (winW >= 400) {
      $('body').css('height', docH - listH - titleH + 20);
      $('.nav').css('height', docH - titleH * 2 - listH - menuH + 20);
      $('#send-content').css('height', docH - titleH * 2 - listH - navdH - menuH + 20);
    }
  }

  function maxH() {
    $('body').css('height', docH);
    $('.nav').css('height', docH - titleH - menuH);
    $('#send-content').css('height', docH - titleH - menuH - navdH);
  }

  function tedH() {
    $('body').animate({
      scrollTop: $('body').offset().top
    }, 50);
    // setTimeout(function () {
    //   document.body.scrollTop = document.body.scrollHeight;
    // }, 50);
  }

  window.onload = function () {
    $('#send-content').trigger("click").focus();
    minH();
    tedH();
    $("body").on("touchmove", function (event) {
      event.preventDefault;
    }, false);
    // 文字计数
    $('#send-content').bind('input propertychange', function () {
      var len = this.value.length
      $('#text-count').text(len);
      if (this.value !== '') {
        $('.nav>div').css('color', '#333');
      } else {
        $('.nav>div').css('color', '#ccc');
      }
    });
  }

  // 标题提示
  $('#send-title').focus(function () {
    if (this.value == '请输入20字以内标题') {
      this.value = '';
      $(this).css('color', '#333');
    }
  });
  $('#send-title').blur(function () {
    if (this.value == '') {
      this.value = '请输入20字以内标题'
    };
    if (this.value == '请输入20字以内标题') {
      $(this).css('color', '#ccc');
    }
  });

  //更多功能
  $('#more').bind('click', function () {
    $('#send-content').blur();
    $('.action-menu').slideDown(50).hide(50);
    $('.action-list').slideUp(50).show(50);
    $('.action-b').css('height', '18.8rem');
    maxH();
    tedH();
  });

  //关闭键盘
  $('#blur').bind('click', function () {
    $('#send-content').blur();
    $('.action-b').css('height', '18.8rem');
    maxH();
    tedH();
  });

  //关闭菜单
  $('#close').bind('click', function () {
    $('.action-list').slideDown(50).hide(50);
    $('.action-menu').slideUp(50).show(50);
    $('.action-b').css('height', '0');
    maxH();
    tedH();
  });

  //点击输入
  $('#send-content').bind('click', function () {
    $('#send-content').focus();
    $('.action-menu').slideUp(0).show(0);
    $('.action-list').slideDown(0).hide(0);
    minH();
    tedH();
  });
}